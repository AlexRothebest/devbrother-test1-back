## Installation

```bash
# install dependencies
$ npm install

# create config file
$ cp src/config.example.ts src/config.ts
```

### Don't forget to tune config file

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
